-- phpMyAdmin SQL Dump
-- version 4.8.2
-- https://www.phpmyadmin.net/
--
-- Host: 127.0.0.1
-- Generation Time: Sep 04, 2018 at 05:25 AM
-- Server version: 10.1.34-MariaDB
-- PHP Version: 7.2.7

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET AUTOCOMMIT = 0;
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `larticles`
--

-- --------------------------------------------------------

--
-- Table structure for table `articles`
--

CREATE TABLE `articles` (
  `artc_id` int(10) UNSIGNED NOT NULL,
  `title` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `body` text COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `articles`
--

INSERT INTO `articles` (`artc_id`, `title`, `body`, `created_at`, `updated_at`) VALUES
(1, 'Voluptates iste amet fuga omnis et impedit.', 'Consequatur placeat quis est. Praesentium illo quam perferendis illo nulla. Officiis deserunt facilis ut esse quas aspernatur dolor. Adipisci quis omnis sed in quia.', '2018-09-03 21:28:11', '2018-09-03 21:28:11'),
(2, 'Recusandae sit illo pariatur eos.', 'Iure est nobis voluptatem. Voluptas perspiciatis occaecati autem qui et omnis quas quo. Placeat sint in iure dolorem. Libero qui doloremque accusamus possimus.', '2018-09-03 21:28:11', '2018-09-03 21:28:11'),
(3, 'Illo amet et aut non provident.', 'Veritatis temporibus rerum error numquam. Iusto dolore facere omnis et. Qui quia maxime minima incidunt ducimus. Incidunt voluptatem ut tenetur.', '2018-09-03 21:28:11', '2018-09-03 21:28:11'),
(4, 'Deserunt asperiores ducimus expedita earum.', 'Dicta doloribus nihil nisi aut vel rerum optio. Quis laudantium sunt sint. Possimus dicta similique incidunt voluptate.', '2018-09-03 21:28:11', '2018-09-03 21:28:11'),
(5, 'Aut dignissimos similique quia.', 'Vitae repellat tempore distinctio et aut et. Molestiae rerum nihil saepe quia. Esse repellendus consequatur ut.', '2018-09-03 21:28:11', '2018-09-03 21:28:11'),
(6, 'Quia ullam autem quod.', 'Autem facere tempora animi aut. Dolores iste necessitatibus quis velit modi cumque non. Nostrum inventore ad voluptas veniam. In repudiandae molestias facere minima harum at optio.', '2018-09-03 21:28:11', '2018-09-03 21:28:11'),
(7, 'Voluptatem quidem autem veritatis delectus.', 'Explicabo ea nesciunt sunt qui. Dolor velit tempore esse dignissimos quo et. Voluptatibus dolorem est ullam fugiat sed est quia amet.', '2018-09-03 21:28:11', '2018-09-03 21:28:11'),
(8, 'Voluptas saepe inventore perspiciatis itaque.', 'Voluptatem optio assumenda et consectetur. Fuga deserunt tenetur minus magnam sit recusandae. Et omnis et inventore adipisci et. Dolorem reiciendis veritatis dolorum.', '2018-09-03 21:28:11', '2018-09-03 21:28:11'),
(9, 'Iste et temporibus minus impedit animi dolores.', 'At consequatur qui eaque atque deleniti. Eum magni quia fuga facere. Et rem officia eius labore totam consequatur soluta.', '2018-09-03 21:28:11', '2018-09-03 21:28:11'),
(10, 'Est iste totam repellendus placeat ut earum sed.', 'Illum quisquam asperiores enim odit aut at. Maxime unde aut voluptates necessitatibus vitae consectetur quis. Iure sapiente neque ab iure quo eius omnis accusamus. Autem commodi libero ullam ipsum.', '2018-09-03 21:28:11', '2018-09-03 21:28:11'),
(11, 'Sunt dolore sit odio labore dolor aspernatur.', 'Exercitationem quos vitae quia qui ea. Provident repellendus qui pariatur ex sint. Omnis vel iusto aspernatur ut id. Quia consectetur suscipit accusamus illum dolor praesentium ut.', '2018-09-03 21:28:11', '2018-09-03 21:28:11'),
(12, 'Repellat consequatur quia fuga odit ut.', 'Nam asperiores aliquid iusto quod. Illum et est a enim sunt et. Facere pariatur reprehenderit alias corporis.', '2018-09-03 21:28:11', '2018-09-03 21:28:11'),
(13, 'Hic aliquam beatae consequatur.', 'Earum beatae qui dolor commodi. Accusamus quidem nam ipsam ipsum aut. Quae voluptatem sunt ipsum. Molestiae qui repellat vitae sit cum quasi.', '2018-09-03 21:28:11', '2018-09-03 21:28:11'),
(14, 'Odio sed rem doloremque dolor autem.', 'Fuga sed voluptas beatae id quia atque asperiores. Error et veniam vero est alias explicabo quam. Iste est rerum nemo deleniti inventore consequatur.', '2018-09-03 21:28:11', '2018-09-03 21:28:11'),
(15, 'Et ea ratione et harum maiores deleniti.', 'Aliquid quisquam eaque dicta excepturi sit accusamus saepe. Deserunt exercitationem cum et qui enim molestiae et. Nihil reprehenderit esse sed consequatur sed molestiae quidem.', '2018-09-03 21:28:11', '2018-09-03 21:28:11'),
(16, 'Aperiam perferendis dolorem ducimus eos.', 'Corporis sit eveniet provident voluptas. Maxime debitis id et qui ut distinctio. Consequatur iure facilis consequatur sunt ut.', '2018-09-03 21:28:11', '2018-09-03 21:28:11'),
(17, 'Et et aperiam natus aut a fuga eum.', 'Dolor ex molestiae consequatur. Corrupti suscipit quod eos qui quia qui aliquid.', '2018-09-03 21:28:11', '2018-09-03 21:28:11'),
(18, 'Ut ad rerum molestias error.', 'Qui et voluptatem dolor repudiandae incidunt vel. Soluta ea dolores et debitis. Rerum voluptatem nemo similique. Voluptate ea pariatur nulla quae natus.', '2018-09-03 21:28:11', '2018-09-03 21:28:11'),
(19, 'Aut et neque vero vitae.', 'Earum non rem asperiores. Eius quaerat autem ut. Ut ut molestias nisi consequatur debitis recusandae.', '2018-09-03 21:28:11', '2018-09-03 21:28:11'),
(20, 'Aperiam est asperiores sapiente et quaerat ut.', 'Odit aspernatur a nostrum amet quod. Sit eveniet blanditiis est architecto ut vel. Optio ipsum autem sint iusto dolorem reprehenderit rem eos.', '2018-09-03 21:28:11', '2018-09-03 21:28:11'),
(21, 'Occaecati ipsum quis ut et.', 'Necessitatibus vel voluptas eveniet voluptatem exercitationem cum corporis. Omnis perferendis aperiam eius consequatur nobis aut iusto provident.', '2018-09-03 21:28:12', '2018-09-03 21:28:12'),
(22, 'Numquam animi maxime officia magni ipsum soluta.', 'Aut et ea magnam quis nostrum est. Tempora omnis et repudiandae mollitia aspernatur adipisci. Totam a eaque tenetur quo facilis dolorem. Culpa sit quaerat suscipit.', '2018-09-03 21:28:12', '2018-09-03 21:28:12'),
(23, 'Ut asperiores ea porro corrupti autem autem.', 'Dolores quas repellat distinctio quo et voluptatem autem. Iure quia et consequatur tempora suscipit qui. Veniam et ullam libero molestiae unde voluptatibus.', '2018-09-03 21:28:12', '2018-09-03 21:28:12'),
(24, 'Culpa sit praesentium inventore.', 'Aliquam tenetur iure sit harum cum enim. Inventore voluptatibus eum et nihil expedita. Perspiciatis et excepturi vitae adipisci. Commodi eum cupiditate non et quis.', '2018-09-03 21:28:12', '2018-09-03 21:28:12'),
(25, 'Quam culpa odio ut deserunt.', 'Labore nemo architecto id modi libero itaque. Quo itaque magni assumenda voluptas eveniet. Consequatur quo sit sed illo. Qui sed et ut alias voluptas est. Nam labore sed id.', '2018-09-03 21:28:12', '2018-09-03 21:28:12'),
(26, 'Sint non et quia qui.', 'Aut vel commodi qui numquam est. Quos itaque quae minima ea expedita modi id. Quasi veritatis dignissimos nihil temporibus.', '2018-09-03 21:28:12', '2018-09-03 21:28:12'),
(27, 'Enim nisi eum nam exercitationem.', 'Corporis quo voluptas aut dignissimos a fugit voluptatem. Deleniti illo ullam amet fugit. Et commodi blanditiis qui provident. Doloremque libero et id est qui.', '2018-09-03 21:28:12', '2018-09-03 21:28:12'),
(28, 'Sint repudiandae modi aut corporis.', 'Nisi placeat repudiandae voluptate corrupti dolores. Asperiores laboriosam iure voluptatem eos necessitatibus tenetur sapiente. Neque sint quis dolores est placeat voluptate.', '2018-09-03 21:28:12', '2018-09-03 21:28:12'),
(29, 'Magni sed nobis voluptates quia soluta.', 'Exercitationem sapiente et molestiae est deserunt aliquam. Fugiat perspiciatis rerum ut placeat. Qui assumenda similique unde sit molestias in.', '2018-09-03 21:28:12', '2018-09-03 21:28:12'),
(30, 'Voluptate commodi dolores enim veritatis.', 'Est illo cum voluptatem tenetur est consequatur doloremque. Explicabo provident quia quaerat veritatis. Sapiente exercitationem nostrum nihil incidunt cumque. Nihil odit quia dolorem ut vitae ut.', '2018-09-03 21:28:12', '2018-09-03 21:28:12');

-- --------------------------------------------------------

--
-- Table structure for table `migrations`
--

CREATE TABLE `migrations` (
  `id` int(10) UNSIGNED NOT NULL,
  `migration` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `batch` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `migrations`
--

INSERT INTO `migrations` (`id`, `migration`, `batch`) VALUES
(1, '2014_10_12_000000_create_users_table', 1),
(2, '2014_10_12_100000_create_password_resets_table', 1),
(3, '2018_09_04_024008_create_articles_tables', 1);

-- --------------------------------------------------------

--
-- Table structure for table `password_resets`
--

CREATE TABLE `password_resets` (
  `email` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `token` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `users`
--

CREATE TABLE `users` (
  `user_id` int(10) UNSIGNED NOT NULL,
  `name` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `email` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `password` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `remember_token` varchar(100) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Indexes for dumped tables
--

--
-- Indexes for table `articles`
--
ALTER TABLE `articles`
  ADD PRIMARY KEY (`artc_id`);

--
-- Indexes for table `migrations`
--
ALTER TABLE `migrations`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `password_resets`
--
ALTER TABLE `password_resets`
  ADD KEY `password_resets_email_index` (`email`);

--
-- Indexes for table `users`
--
ALTER TABLE `users`
  ADD PRIMARY KEY (`user_id`),
  ADD UNIQUE KEY `users_email_unique` (`email`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `articles`
--
ALTER TABLE `articles`
  MODIFY `artc_id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=31;

--
-- AUTO_INCREMENT for table `migrations`
--
ALTER TABLE `migrations`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=4;

--
-- AUTO_INCREMENT for table `users`
--
ALTER TABLE `users`
  MODIFY `user_id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT;
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
